# Network Re-starter

## Description
check if you could access google.com and restart interface if not.

## Prerequisites
Works with Ubuntu 18.04. Might work with other versions but untested.

## CRON
Must be `sudo`

`sudo crontab -e` possible values `* 8-17 * * MON-FRI /scripts/check_and_restart_network.sh`
