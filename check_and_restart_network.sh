#!/bin/bash
myHost="google.com"
COUNT=4

echo "Checking network connection at $(date)" >> /var/log/check_and_restart_network.log
count=$(timeout 10 ping -c $COUNT $myHost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')

# check if no service/network available of 100% lost
if [ "$count" == "" ] || [ "$count" == "0" ]; then
  echo "Host : $myHost is down (ping failed) at $(date)" >> /var/log/check_and_restart_network.log
  systemctl restart NetworkManager.service
fi
