#!/bin/bash
myHost="google.com"
COUNT=4

echo "Checking network connection at $(date)" >> /var/log/check_and_restart_network.log
count=$(timeout 10 ping -c $COUNT $myHost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
vpn_status=$(/usr/bin/nordvpn status | grep  "Status: Connected")

# check if no service/network available of 100% lost
if [ "$count" == "" ] || [ "$count" == "0" ] || [ -z "$vpn_status" ]; then
  echo "Connection is down $(date)" >> /var/log/check_and_restart_network.log
  /usr/bin/nordvpn d
  #/usr/bin/nordvpn rate 1
  /usr/bin/nordvpn c >> /var/log/check_and_restart_network.log
fi
